       IDENTIFICATION DIVISION.
       PROGRAM-ID. DATA5.
       AUTHOR. KHUMMEUNG WATTANASAROJ.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 GRADE-DATA PIC X(90) VALUE "62160244KHUMMEUNG       886243593
      -    "D+886244593B 886245593A 886323593A 886342593A 886344593A".
       01 GRADE.
           03 STU-ID       PIC 9(8).
           03 STU-NAME     PIC X(16).
           03 SUB1.
               05 SUB-CODE1    PIC 9(8).
               05 SUB-UNIT1    PIC 9.
               05 SUB-GRADE1   PIC X(2).
           03 SUB2.
               05 SUB-CODE2    PIC 9(8).
               05 SUB-UNIT2    PIC 9.
               05 SUB-GRADE2   PIC X(2).
           03 SUB3.
               05 SUB-CODE3    PIC 9(8).
               05 SUB-UNIT3    PIC 9.
               05 SUB-GRADE3   PIC X(2).
           03 SUB4.
               05 SUB-CODE4    PIC 9(8).
               05 SUB-UNIT4    PIC 9.
               05 SUB-GRADE4   PIC X(2).
           03 SUB5.
               05 SUB-CODE5    PIC 9(8).
               05 SUB-UNIT5    PIC 9.
               05 SUB-GRADE5   PIC X(2).
           03 SUB6.
               05 SUB-CODE6    PIC 9(8).
               05 SUB-UNIT6    PIC 9.
               05 SUB-GRADE6   PIC X(2).
       PROCEDURE DIVISION.
       Begin.
           MOVE GRADE-DATA TO GRADE.
           DISPLAY GRADE

           DISPLAY "SUBJECT 1"
           DISPLAY "CODE  : " SUB-CODE1
           DISPLAY "UNIT  : " SUB-UNIT1
           DISPLAY "GRADE : " SUB-GRADE1

           DISPLAY "SUB 3 : "SUB3
           .
